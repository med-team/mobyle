#! /usr/bin/env python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi

def process(self):
    try:
        if (self.cfg.anonymousSession()!='no'):
            self.session = self.sessionFactory.getAnonymousSession()
            self.sessionKey = self.session.getKey()
        else: 
            self.session = None
            self.sessionKey = None
        self.read_session()
        self.jsonMap['ok'] = str(True)
        self.jsonMap['msg'] = str("You have been signed out.")
    except Exception, e:
        self.jsonMap['ok'] = str(False)        
        self.jsonMap['msg'] = str(e)

if __name__ == "__main__":
    mb_cgi.JSONCGI(processFunction=process,useSession=True)