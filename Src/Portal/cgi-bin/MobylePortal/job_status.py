#! /usr/bin/env python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi
from Mobyle.JobFacade import JobFacade

def process(self):
    j = JobFacade.getFromJobId(self.request.getfirst('jobId',None))
    status = j.getStatus()
    self.jsonMap = {
            'status': str( status ),
            'msg': status.message
           }
    
if __name__ == "__main__":
    mb_cgi.JSONCGI(processFunction=process)