#! /usr/bin/env python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi
from Mobyle.JobFacade import JobFacade
from Mobyle.Registry import registry

def process(self):
    if(self.request.has_key('jobId')):
        j = JobFacade.getFromJobId(self.request.getfirst('jobId',None))
    elif(self.request.has_key('jobPid')):
        pid = registry.getJobURL(self.request.getfirst('jobPid',None))
        j = JobFacade.getFromJobId(pid)
    self.jsonMap = j.killJob()
    
if __name__ == "__main__":
    mb_cgi.JSONCGI(processFunction=process)