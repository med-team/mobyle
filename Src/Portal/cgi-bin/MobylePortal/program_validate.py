#!/usr/bin/env python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi
from Mobyle.Validator import Validator
from Mobyle.JobFacade import JobFacade
from Mobyle.MobyleError import MobyleError

def process(self):
    val = Validator(self.request.getfirst("type","program"),docString = self.request.getfirst("programXmlString"),runRNG_Jing=True)
    val.run()
    self.jsonMap = {'validates':val.valOk}
    if not val.valOk:
        self.jsonMap['validation_errors'] = ((val.rng_JingErrors or val.rngErrors) or []) + val.schErrors
    if val.valOk:
        self.service = val.service
        try:
            j = JobFacade.getFromService(service=val.service)
        except MobyleError, err:
            self.jsonMap['job_simulation_errormsg'] = err.message
            mb_cgi.c_log.error(err.message, exc_info=True)
            return
        resp = j.create(self.request, session=self.session, simulate=True)
        if resp.has_key('errormsg'):
            self.jsonMap['errormsg'] = resp['errormsg']
            if resp.has_key('errorparam'):
                self.jsonMap['errorparam'] = resp['errorparam']
        else:
            self.jsonMap['job_simulation'] = j.getExecutionDetails()        
if __name__ == "__main__":
    mb_cgi.JSONCGI(processFunction=process,useSession=True)
