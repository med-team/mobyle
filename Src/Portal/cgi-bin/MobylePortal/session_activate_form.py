#! /usr/bin/env python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi

def process(self):
    if self.session.isAuthenticated():
        self.template_file = 'session_activate_auth.html'
    else:
        import random
        self.key = random.randint(1,1000)
        self.template_file = 'session_activate_ano.html'

if __name__ == "__main__":
    mb_cgi.TALCGI(processFunction=process,useSession=True)
