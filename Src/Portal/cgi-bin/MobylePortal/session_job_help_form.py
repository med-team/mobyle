#! /usr/bin/env python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi

def process(self):
    self.jobId = self.request.getfirst('id',None)
    self.message = self.request.getfirst('message',None)
    self.param = self.request.getfirst('param',None)
    self.email = self.session.getEmail()
    self.template_file = 'session_job_help.html'

if __name__ == "__main__":
    mb_cgi.TALCGI(processFunction=process,useSession=True)
