#! /usr/bin/env python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi
from Mobyle.MobyleError import UserValueError
import Mobyle.Net

def process(self):
    self.email = self.request.getfirst('email',None)    
    if self.email:
        try:
            self.session.setEmail(Mobyle.Net.EmailAddress( self.email))
            self.jsonMap['ok'] = str(True)
        except UserValueError:
            self.jsonMap['ok'] = str(False)
            self.jsonMap['errormsg'] = "Invalid e-mail, please provide a valid address"            
    else:
        self.jsonMap['ok'] = str(False)
        self.jsonMap['errormsg'] = "Please specify an e-mail"
  


if __name__ == "__main__":
    mb_cgi.JSONCGI(processFunction=process,useSession=True)