#! /usr/bin/env python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi
from Mobyle.JobFacade import JobFacade
import Mobyle.Net
import time #@UnresolvedImport
from Mobyle.MobyleError import MobyleError

def process(self):
    self.email = self.request.getfirst('email',None)
    if self.email:
        self.session.setEmail(Mobyle.Net.EmailAddress( self.email) )
    try:
        j = JobFacade.getFromService(programUrl=self.request.getfirst('programName',None),workflowUrl=self.request.getfirst('workflowUrl',None))
        j.email_notify = self.request.getfirst('_email_notify','auto')
        self.jsonMap = j.create(self.request, None, self.session)
        if self.jsonMap.has_key('id') and not(self.jsonMap.has_key('errormsg')):
            self.jsonMap = j.addJobToSession(self.session, self.jsonMap)
            job = self.session.getJob(self.jsonMap['id'])
            jobdatestring = time.strftime( "%x    %X", job['date'])
            self.jsonMap['title'] = job['programName'] + ' - ' + jobdatestring
    except MobyleError, err:
        self.jsonMap = {'errormsg': err.message}
        if hasattr(err, 'param'):
            self.jsonMap['errorparam']=err.param.getName()            
        return
 
if __name__ == "__main__":
    mb_cgi.JSONCGI(processFunction=process,useSession=True)
