<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" omit-xml-declaration="yes" indent="yes" />
	
	<xsl:param name="workflow_id"></xsl:param>

	<xsl:key name="get_node_id" match="/graphml/graph/node" use="@id" />

	<xsl:template match="/">
		<xsl:for-each select="graphml/graph">
			<workflow>
				<xsl:attribute name="id">
                    <xsl:value-of select="@id" />
                </xsl:attribute>
                <head>
                	<doc>
                		<xsl:apply-templates select="data" />
                 	</doc>
                </head>
				<flow>
					<xsl:for-each select="node">
						<task>
							<xsl:attribute name="id">
                                <xsl:value-of select="@id" />
                            </xsl:attribute>
							<xsl:attribute name="service">
                                <xsl:value-of select="data[@key='name']" />

                            </xsl:attribute>
							<xsl:apply-templates select="data[@key='xy']" />
							<xsl:apply-templates select="data[@key='position']" />
						</task>
					</xsl:for-each>
  				<xsl:apply-templates select="node/port[data[@key='output' and text()='true']]" mode="link" />
					<xsl:for-each select="edge">
						<link>
							<xsl:attribute name="id">
                            <xsl:choose>
                                <xsl:when test="@id !=''">
                                    <xsl:value-of select="@id" />
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="position()" />
                                </xsl:otherwise>
                            </xsl:choose>

                        </xsl:attribute>
							<xsl:attribute name="fromParameter">
                            <xsl:value-of select="@sourcePort" />
                        </xsl:attribute>
							<xsl:attribute name="fromTask">
                            <xsl:value-of
								select="key('get_node_id',@source)/@id" />
                        </xsl:attribute>
							<xsl:attribute name="toParameter">
                            <xsl:value-of select="@targetPort" />
                        </xsl:attribute>
							<xsl:attribute name="toTask">
                            <xsl:value-of
								select="key('get_node_id',@target)/@id" />
                        </xsl:attribute>
						</link>
					</xsl:for-each>
				</flow>
				<parameters>
					<xsl:apply-templates select="node/port[data[@key='output' and text()='true']]" mode="parameter"/>
				</parameters>
			</workflow>
		</xsl:for-each>

	</xsl:template>


	<xsl:template match="port[data[@key='output' and text()='true']]"
		mode="parameter">
		<parameter isout="1">
			<xsl:attribute name="id">
        <xsl:value-of select="concat('workflow_',../@id,'_',@name)" />
      </xsl:attribute>
			<name>
				<xsl:value-of select="concat('workflow_',../@id,'_',@name)" />
			</name>
			<prompt></prompt>
		</parameter>
	</xsl:template>

	<xsl:template match="port[data[@key='output' and text()='true']]"
		mode="link">
		<link>
			<xsl:attribute name="id">
                        <xsl:value-of select="../@id" />
                        <xsl:text>_</xsl:text>
                        <xsl:value-of select="@name" />
                        <xsl:text>_to_</xsl:text>
								        <xsl:value-of select="concat('workflow_',../@id,'_',@name)" />
                    </xsl:attribute>
			<xsl:attribute name="fromParameter">
                        <xsl:value-of select="@name" />
                    </xsl:attribute>
			<xsl:attribute name="fromTask">
                        <xsl:value-of select="../@id" />
                    </xsl:attribute>
			<xsl:attribute name="toParameter">
        <xsl:value-of select="concat('workflow_',../@id,'_',@name)" />
      </xsl:attribute>
		</link>
	</xsl:template>
	
	<xsl:template match="data[@key='title']">
		<title><xsl:value-of select="text()" /></title>
	</xsl:template>

	<xsl:template match="data[@key='description']">
		<description>
			<xsl:choose>
				<xsl:when test="not(*)">
					<text><xsl:value-of select="text()" /></text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:copy-of select="*" />
				</xsl:otherwise>
			</xsl:choose>
		</description>
	</xsl:template>

	<xsl:template match="data[@key='comment']">
		<comment>
			<xsl:choose>
				<xsl:when test="not(*)">
					<text><xsl:value-of select="text()" /></text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:copy-of select="*" />
				</xsl:otherwise>
			</xsl:choose>
		</comment>
	</xsl:template>
	
	<xsl:template match="data[@key='xy']">
		<position><xsl:value-of select="text()" /></position>
	</xsl:template>

	<xsl:template match="text" >
		<xsl:value-of select="text()" />
	</xsl:template>
	
</xsl:stylesheet>