#! /usr/bin/env python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi
    
def process( self ):
    jobsMap = {}
    dataMap = {}
    workflows = {}
    if self.session:
        tmpjobs = self.session.getAllJobs()
        jobsMap = {}
        for job in tmpjobs:
            jl = mb_cgi.get_formatted_job_entries(job)
            for entry in jl:
                jobsMap[entry['jobID']] = entry
        data = self.session.getAllData()
        for dataFile in data:
            dataType = dataFile['Type'].getDataType().name
            bioTypes = []
            for bt in dataFile['Type'].getBioTypes():
                bioTypes.append(str(bt))
            userModes = []
            for im in dataFile['inputModes']:
                userModes.append(im)
            data_format = dataFile['Type'].getDataFormat()
            dataMap[dataFile['dataName']] = {'userName':dataFile['userName'], 'dataType': dataType, 'bioTypes': bioTypes, 'userModes': userModes, 'title':dataFile['dataBegin'], 'size': '%.2f'%(float(dataFile['size'])/1000), 'format': data_format}
        workflows = self.session.getBMPSWorkflows()
    self.jsonMap = {'jobs': jobsMap, 'data': dataMap, 'workflows':workflows}

if __name__ == "__main__":
    mb_cgi.JSONCGI(processFunction=process,useSession=True)