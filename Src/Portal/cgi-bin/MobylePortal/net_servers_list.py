#! /usr/bin/env python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi
from Mobyle.Registry import registry

def process( self ):
    for server in registry.servers:
        self.jsonMap[server.name] = {
                                     'url':server.url,
                                     'help':server.help,
                                     'repository':server.repository,
                                     'programs':[program.name for program in server.programs]
                                     }
        

if __name__ == "__main__":
    il = mb_cgi.JSONCGI(processFunction=process)