#! /usr/bin/env python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi

def process(self):
    ok = self.session.checkCaptchaSolution(self.request.getfirst('solution'))
    if not(ok):
        self.jsonMap['errormsg'] = "Wrong answer, try again"
        self.jsonMap['reset'] = True
    self.read_session()
    self.jsonMap['ok'] = str(ok)

if __name__ == "__main__":
    mb_cgi.JSONCGI(processFunction=process,useSession=True)