<?xml version="1.0" encoding="utf-8"?>
<!-- 
	ident.xsl stylesheet
	Authors: Hervé Ménager
-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:xhtml="http://www.w3.org/1999/xhtml">
	<!-- transform a deployed mobyle xml to remove the interface tags (useless so far in BMW) and 
		modify vdef values according to the default	values set in the workflow template -->

	<xsl:param name="task_id" />	

	<xsl:param name="workflow_url" />

	<xsl:template match="@*|node()|text()" priority="-1">
		<xsl:call-template name="nodeCopy" />
	</xsl:template>

	<xsl:template name="nodeCopy">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()|text()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="/*">
		<xsl:copy>
			<xsl:attribute name="id"><xsl:value-of select="$task_id"/></xsl:attribute>
			<xsl:apply-templates select="@*|node()|text()"/>
		</xsl:copy>
	</xsl:template>		

	<xsl:template match="interface" />

	<xsl:template match="vdef">
		<xsl:call-template name="vdef_replacement" />
	</xsl:template>		

  <xsl:template name="vdef_replacement">
		<xsl:choose>
			<xsl:when test="document($workflow_url)//task[@id=$task_id]/inputValue[@name=current()/ancestor-or-self::parameter/name/text()]/text()">
				<vdef><value><xsl:value-of select="document($workflow_url)//task[@id=$task_id]/inputValue[@name=current()/ancestor-or-self::parameter/name/text()]/text()" /></value></vdef>
			</xsl:when>
			<xsl:when test="document($workflow_url)//task[@id=$task_id]/inputValue[@name=current()/ancestor-or-self::parameter/name/text()]/@reference">
				<vdef>
					<ref safeName="{document($workflow_url)//task[@id=$task_id]/inputValue[@name=current()/ancestor-or-self::parameter/name/text()]/text()}" 
					     userName="{document($workflow_url)//task[@id=$task_id]/inputValue[@name=current()/ancestor-or-self::parameter/name/text()]/@userName}"
					     srcUrl="{document($workflow_url)//task[@id=$task_id]/inputValue[@name=current()/ancestor-or-self::parameter/name/text()]/@reference}" />
				</vdef>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy-of select="." />
			</xsl:otherwise>
		</xsl:choose>
  </xsl:template>

	<xsl:template match="parameter[not(vdef)]">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()|text()" />
			<xsl:call-template name="vdef_replacement" />
		</xsl:copy>		
	</xsl:template>	

</xsl:stylesheet>
