#! /usr/bin/env python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi
from Mobyle.Registry import registry

def process( self ):
    if self.session:
        pids = self.request.getlist('id')
        for pid in pids:
            self.session.removeJob(registry.getJobURL(pid))
    self.jsonMap['ok'] = str(True)

if __name__ == "__main__":
    mb_cgi.JSONCGI(processFunction=process,useSession=True)
