#! /usr/bin/env python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi
import Mobyle.Net

def process(self):
    try:
        assert self.request.getfirst('email'), "Please provide an e-mail"
        assert self.request.getfirst('password'), "Please provide a password"
        assert self.request.getfirst('key'), "Please provide an activation key"
    except AssertionError, ae:
        self.error_msg = str(ae)
        return
    try:
        email = Mobyle.Net.EmailAddress(self.request.getfirst('email'))
        password = self.request.getfirst('password')
        self.session = self.sessionFactory.getAuthenticatedSession(email,password)
        self.sessionKey = self.session.getKey()
        self.read_session()
        self.session.confirmEmail(self.request.getfirst('key').strip())
        self.jsonMap['ok'] = str(True)
    except Exception, e:
        self.jsonMap['ok'] = str(False)
        self.jsonMap['msg'] = str(e)

if __name__ == "__main__":
    mb_cgi.JSONCGI(processFunction=process,useSession=True)