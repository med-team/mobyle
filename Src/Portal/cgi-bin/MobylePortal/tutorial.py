#! /usr/bin/env python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi
from Mobyle.Registry import registry

def process(self):
    self.xslParams = {}
    try:
        def f(x): return x and x != '.'
        id = filter(f,self.request.getfirst('id').partition('.'))
        if len(id)==1:
            self.service = registry.serversByName['local'].tutorialsByName[id[0]]            
        else:
            self.service = registry.serversByName[id[0]].tutorialsByName[id[1]]            
        if not(self.service.server.name=='local'):
            self.xslParams['remoteServerName']="'"+self.service.server.name+"'"
    except Exception:
        # program does not exist
        self.error_msg = "Tutorial %s cannot be found on this server." % self.request.getfirst('id','')
        self.error_title = "Tutorial Not found"
        return
    if self.service.disabled:
        # program disabled
        self.error_msg = "this tutorial has been disabled on this server"
        self.error_title = "Tutorial Disabled"
    elif not(self.service.authorized):
        # program restricted
        self.error_msg = "the access to this tutorial is restricted on this server"
        self.error_title = "Tutorial Unauthorized"
    else:
        # ok
        self.xmlUrl = self.service.path
    self.xslPipe = [
                    (self.cfg.portal_path()+"/xsl/tutorial.xsl",
                        {'programUri':"'"+self.service.url+"'",
                         'programPID':"'"+self.service.pid+"'",
                         'codebase':"'"+self.cfg.repository_url()+"/'"}),
                    (self.cfg.portal_path()+"/xsl/remove_ns.xsl",{}) # remove xhtml namespace junk
                    ]
    if self.service.server.name=='local':
        self.title = self.service.name
    else:
        self.title = self.request.getfirst('id')
        tmptitle = self.title.split('.')
        tmptitle.reverse()
        self.title = '@'.join(tmptitle)

if __name__ == "__main__":
    il = mb_cgi.XSLCGI(processFunction=process,useSession=True)
