#! /usr/bin/env python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi
from Mobyle.Registry import registry

def process(self):
    self.mime_type = 'image/svg+xml'
    self.jobID = registry.getJobURL(self.request.getfirst('id'))
    self.jobPID = registry.getJobPID(self.jobID)
    self.xslParams = {}
    self.xmlUrl = self.jobID + '/index.xml'
    self.xslPipe = [
                    (self.cfg.portal_path()+"/xsl/job_graph.xsl",{'jobPID':"'"+self.jobPID+"'"})
                    ]

if __name__ == "__main__":
    il = mb_cgi.XSLCGI(processFunction=process)