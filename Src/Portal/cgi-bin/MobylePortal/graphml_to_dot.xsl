<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" omit-xml-declaration="yes" indent="yes" />

	<xsl:key name="get_node_id" match="graphml/graph/node" use="@id" />

	<xsl:template match="/">
		digraph GRAPH_0 {
		graph [rankdir=LR];
		node [label="\N", shape=record, style=filled];
		<xsl:for-each select="graphml/graph/node">
			<xsl:variable name="node_id" select="@id" />
			<xsl:variable name="node_name" select="@id" />
			<xsl:value-of select="$node_id" disable-output-escaping="no" />
			[color="#EEEEE", shape=plaintext,label=
			<xsl:text disable-output-escaping="yes"> &lt; </xsl:text>
			<TABLE BORDER="0" CELLBORDER="0" CELLSPACING="0">
				<TR>
					<TD HREF="#">
						<xsl:attribute name="TITLE">
                            <xsl:value-of select="$node_name" />
                        </xsl:attribute>

						<TABLE BORDER="0" CELLBORDER="0" CELLSPACING="0">
							<TR>
								<TD>
									<xsl:if test="port/data[@key ='type'] = 'pipeIn'">
										<xsl:call-template name="process_param">
											<xsl:with-param name="node_name" select="$node_name" />
											<xsl:with-param name="port_type" select="'pipeIn'" />
										</xsl:call-template>
									</xsl:if>
								</TD>
								<TD>
									<xsl:value-of select="$node_name" />
								</TD>
								<TD>
									<xsl:if test="port/data[@key ='type'] = 'pipeOut'">
										<xsl:call-template name="process_param">
											<xsl:with-param name="node_name" select="$node_name" />
											<xsl:with-param name="port_type" select="'pipeOut'" />
										</xsl:call-template>
									</xsl:if>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
			<xsl:text disable-output-escaping="yes">></xsl:text>
			];
		</xsl:for-each>
		<xsl:for-each select="graphml/graph/edge">
			<xsl:value-of select="key('get_node_id',@source)/@id" />
			:
			<xsl:value-of select="@sourcePort" />
			<xsl:text disable-output-escaping="yes">-></xsl:text>
			<xsl:value-of select="key('get_node_id',@target)/@id" />
			:
			<xsl:value-of select="@targetPort" />
			:c
			<xsl:text>&#10;</xsl:text>
		</xsl:for-each>
		}
	</xsl:template>
	<xsl:template name="process_param">
		<xsl:param name="node_name" />
		<xsl:param name="port_type" />
		<TABLE BORDER="0" CELLBORDER="0" CELLSPACING="0">
			<xsl:for-each select="port">
				<xsl:if test="data[@key ='type'] = $port_type">
					<TR>
						<TD>
							<xsl:attribute name="TITLE">
                                <xsl:value-of
								select="concat($node_name,':', @name)" />
                            </xsl:attribute>
							<xsl:attribute name="PORT">
                                <xsl:value-of select="@name" />
                        </xsl:attribute>
							*
						</TD>
					</TR>
				</xsl:if>
			</xsl:for-each>
		</TABLE>
	</xsl:template>


</xsl:stylesheet>

