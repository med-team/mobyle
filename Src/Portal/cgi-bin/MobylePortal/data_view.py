#! /usr/bin/env python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi

def process( self ):
    data = self.session.getData(self.request.getfirst('id'))
    self.title = data['userName']
    self.xmlUrl = self.session.getDir() + '/' + '.session.xml'
    self.xslPipe = [
                    (self.cfg.portal_path()+"/xsl/bookmark.xsl",
                        {'dataId':"'"+self.request.getfirst('id')+"'",
                         'sessionUrl':"'"+self.session.url+"'",
                         'inputModes': "'"+' '.join(data['inputModes'])+"'"
                         })
                    ]
              
if __name__ == "__main__":
    mb_cgi.XSLCGI(processFunction=process,useSession=True)