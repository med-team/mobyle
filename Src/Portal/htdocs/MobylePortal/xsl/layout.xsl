<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- 
	layout.xsl stylesheet
	Authors: Hervé Ménager
-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xhtml="http://www.w3.org/1999/xhtml"  xmlns:mb="http://www.mobyle.fr">

	<xsl:namespace-alias stylesheet-prefix="mb" result-prefix="#default"/>	

	<xsl:param name="type" />

	<xsl:include href="ident.xsl" />	

	<xsl:template match="head">
		<xsl:if test="not(interface[@type=$type])">
			<xsl:copy>
				<xsl:apply-templates select="@*|node()" />
				<xsl:if test="//interface[@type=$type]">
					<mb:interface type="{$type}" generated="true">
						<xsl:choose>
							<xsl:when test="layout[@type='$type']">
								<xsl:apply-templates select="layout[@type='$type']" mode="interfaceBuild" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:apply-templates select="/*/parameters/*" mode="interfaceBuild" />
							</xsl:otherwise>
						</xsl:choose>
					</mb:interface>					
				</xsl:if>
			</xsl:copy>
		</xsl:if>
		<xsl:if test="interface[@type=$type]">
			<xsl:copy-of select="."/>>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="paragraph" mode="interfaceBuild">
		<xsl:if test=".//interface[@type=$type]" >
			<xhtml:fieldset class="minimizable">
				<xsl:attribute name="data-paragraphname"><xsl:value-of select="name/text()"/></xsl:attribute>
				<xhtml:legend><xsl:value-of select="prompt/text()" /></xhtml:legend>
				<xhtml:div>
				<xsl:choose>
					<xsl:when test="layout">
						<xsl:apply-templates select="layout" mode="interfaceBuild" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="parameters" mode="interfaceBuild" />
					</xsl:otherwise>
				</xsl:choose>
				</xhtml:div>
			</xhtml:fieldset>
		</xsl:if>
	</xsl:template>

	<xsl:template match="parameter" mode="interfaceBuild">
		<xsl:apply-templates select="interface" mode="interfaceBuild" />
	</xsl:template>	

	<xsl:template match="interface/xhtml:*" mode="interfaceBuild">
		<xsl:if test="../@type[.=$type]">
			<xsl:element name="{name(.)}" namespace="{namespace-uri(.)}" use-attribute-sets="parametername" >
				<xsl:copy-of select="@*|*|text()"/>
			</xsl:element>
		</xsl:if>
	</xsl:template>	

	<xsl:template match="layout">
	<xsl:copy-of select="." />
	</xsl:template>
	
	<xsl:template match="layout" mode="interfaceBuild">
		<xsl:choose>
			<xsl:when test="name(..)='hbox'">
				<xhtml:td>
					<xhtml:table>
						<xsl:apply-templates select="*" mode="interfaceBuild" />
					</xhtml:table>
				</xhtml:td>        
			</xsl:when>
			<xsl:when test="name(..)='vbox'">
				<xhtml:tr>
					<xhtml:td>
						<xhtml:table>
							<xsl:apply-templates select="*" mode="interfaceBuild" />
						</xhtml:table>
					</xhtml:td>        
				</xhtml:tr>
			</xsl:when>
			<xsl:otherwise>
				<xhtml:table>
					<xsl:apply-templates select="*" mode="interfaceBuild" />
				</xhtml:table>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="hbox" mode="interfaceBuild">
		<xhtml:tr>
			<xsl:apply-templates select="*" mode="interfaceBuild" />
		</xhtml:tr>
	</xsl:template>
	
	<xsl:template match="vbox">
		<xsl:apply-templates select="*" mode="interfaceBuild" />
	</xsl:template>
	
	
	<xsl:template match="box" mode="interfaceBuild">
		<xsl:choose>
			<xsl:when test="name(..)='hbox'">
				<xhtml:td>
					<xsl:apply-templates select="//parameter[name/text()=current()/text()]" mode="interfaceBuild" />
				</xhtml:td>        
			</xsl:when>
			<xsl:when test="name(..)='vbox'">
				<xhtml:tr>
					<xhtml:td>
						<xsl:apply-templates select="//parameter[name/text()=current()/text()]" mode="interfaceBuild" />
					</xhtml:td>        
				</xhtml:tr>
			</xsl:when>      
		</xsl:choose>
	</xsl:template>
	
	<xsl:attribute-set name="parametername">
		<xsl:attribute name="data-{local-name(current()/../..)}name">
			<xsl:value-of select="../../name/text()"/>
		</xsl:attribute>
	</xsl:attribute-set>	
	
</xsl:stylesheet>