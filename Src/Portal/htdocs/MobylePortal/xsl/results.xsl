<?xml version="1.0" encoding="utf-8"?>
<!-- 
	form_parameters.xsl stylesheet
	Authors: Hervé Ménager
-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xhtml="http://www.w3.org/1999/xhtml">
	
	<xsl:include href="ident.xsl" />

	<xsl:param name="jobUrl" />
	
	<xsl:template match="parameters">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
			<xsl:if test="(/program) and not(ancestor::paragraph) and not(//parameter[(@isstdout='true' or @isstdout='1')])">
				<!-- Adding stdout parameter if not specified in the program description -->
				<parameter isstdout="true">
					<name>stdout</name>
					<prompt>Standard output</prompt>
					<type>
						<datatype>
							<class>Report</class>
						</datatype>
					</type>
					<interface type="job_output" generated="true">
						<xhtml:div data-parametername="stdout">
						</xhtml:div>
					</interface>
					<filenames>
						<code proglang="perl">"<xsl:value-of select="/program/head/name/text()" />.out"</code>
						<code proglang="python">"<xsl:value-of select="/program/head/name/text()" />.out"</code>
					</filenames>
				</parameter>
			</xsl:if>
			<!-- Adding stderr parameter -->
			<xsl:if test="(/program) and not(ancestor::paragraph)">
				<parameter isout="true">
					<name>stderr</name>
					<prompt>Standard error</prompt>
					<type>
						<datatype>
							<class>Report</class>
						</datatype>
					</type>
					<interface type="job_output" generated="true">
						<xhtml:div data-parametername="stderr">
						</xhtml:div>
					</interface>
					<filenames>
						<code proglang="perl">"<xsl:value-of select="/program/head/name/text()" />.err"</code>
						<code proglang="python">"<xsl:value-of select="/program/head/name/text()" />.err"</code>
					</filenames>
				</parameter>
			</xsl:if>
		</xsl:copy>
	</xsl:template>	

	<xsl:template match="parameter[(@isout or @isstdout) and count(ancestor-or-self::*/interface[@type='job_output'])=0]">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
			<interface generated="true" type="job_output">
				<div xmlns="http://www.w3.org/1999/xhtml" data-parametername="{name/text()}">
				</div>
			</interface>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="parameter[not(@isout or @isstdout) and count(ancestor-or-self::*/interface[@type='job_input'])=0]">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
			<interface generated="true" type="job_input">
				<div xmlns="http://www.w3.org/1999/xhtml" data-parametername="{name/text()}">
				</div>
			</interface>
		</xsl:copy>
	</xsl:template>		

</xsl:stylesheet>
