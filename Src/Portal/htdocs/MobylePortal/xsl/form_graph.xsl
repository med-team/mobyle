<?xml version="1.0" encoding="utf-8"?>
<!-- 
	form_graph.xsl stylesheet
	Authors: Hervé Ménager
-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:svg="http://www.w3.org/2000/svg">

	<xsl:include href="ident.xsl" />

	<xsl:template match="/">
		<xsl:apply-templates select="workflow/head/interface[@type='graph']/svg:svg"/>
	</xsl:template>

</xsl:stylesheet>
