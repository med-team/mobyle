<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- 
	annotate.xsl stylesheet
	Authors: Herv� M�nager
-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml">

	<xsl:include href="ident.xsl" />

	<!-- processing parameters to annotate them with necessary classes and transform nesting labels into fieldsets --> 
	<xsl:template match="xhtml:label[not(xhtml:label) and (not(xhtml:input[@type='radio']))]">
		<xsl:variable name="param-name" select="current()//xhtml:*/@name" />
		<xsl:element name="label" use-attribute-sets="param">
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates select="text()" />
			<xsl:apply-templates select="//parameter[(name/text()=$param-name)]/type" mode="label"/>
			<xsl:apply-templates select="//parameter[(name/text()=$param-name)]/comment" mode="ajaxLink"/>			
			<xsl:apply-templates select="//parameter[(name/text()=$param-name)]/example" mode="ajaxLink"/>			
			<xsl:apply-templates select="*" />
			<xsl:apply-templates select="//parameter[(name/text()=$param-name)]/comment" mode="ajaxTarget"/>
			<xsl:apply-templates select="//parameter[(name/text()=$param-name)]/example" mode="ajaxTarget"/>
		</xsl:element>
	</xsl:template>	

	<xsl:template match="xhtml:label[xhtml:label]">
		<xsl:element name="fieldset" use-attribute-sets="param">
			<xsl:apply-templates select="@*" />
			<legend>
				<xsl:value-of select="text()"/>
				<xsl:apply-templates select="//parameter[(name/text()=current()//xhtml:*/@name)]/type" mode="label"/>
				<xsl:apply-templates select="//parameter[(name/text()=current()//xhtml:*/@name)]/comment" mode="ajaxLink"/>			
			</legend>
			<xsl:apply-templates select="//parameter[(name/text()=current()//xhtml:*/@name)]/comment" mode="ajaxTarget" />			
			<xsl:apply-templates select="*" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="xhtml:fieldset[@data-paragraphname]">
		<xsl:element name="fieldset">
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates select="xhtml:legend" />
			<xsl:apply-templates select="//paragraph[(name/text()=current()/@data-paragraphname)]/comment" mode="ajaxTarget"/>
			<xsl:apply-templates select="//paragraph[(name/text()=current()/@data-paragraphname)]/example" mode="ajaxTarget"/>
			<xsl:apply-templates select="*[local-name()!='legend']" />
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="xhtml:fieldset[@data-parametername]">
		<xsl:element name="fieldset" use-attribute-sets="param">
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates select="xhtml:legend" />
			<xsl:apply-templates select="//parameter[(name/text()=current()/@data-parametername)]/comment" mode="ajaxTarget"/>          
			<xsl:apply-templates select="//parameter[(name/text()=current()/@data-parametername)]/example" mode="ajaxTarget"/>          
			<xsl:apply-templates select="*[local-name()!='legend']" />
		</xsl:element>
	</xsl:template> 
	
	<xsl:template match="xhtml:fieldset[@data-paragraphname]/xhtml:legend">
		<legend>
			<xsl:apply-templates select="@*|node()|text()" />
			<xsl:apply-templates select="//paragraph[(name/text()=current()/../@data-paragraphname)]/comment" mode="ajaxLink"/>         
			<xsl:apply-templates select="//paragraph[(name/text()=current()/../@data-paragraphname)]/example" mode="ajaxLink"/>         
		</legend>       
	</xsl:template>     
	
	<xsl:template match="xhtml:fieldset[@data-parametername]/xhtml:legend">
		<legend>
			<xsl:apply-templates select="@*|node()|text()" />
			<xsl:apply-templates select="//parameter[(name/text()=current()/../@data-parametername)]/comment" mode="ajaxLink"/>         
			<xsl:apply-templates select="//parameter[(name/text()=current()/../@data-parametername)]/example" mode="ajaxLink"/>         
		</legend>       
	</xsl:template> 
	
	<xsl:template match="xhtml:div[@data-parametername]">
		<xsl:element name="div" use-attribute-sets="param">
			<xsl:apply-templates select="@*|node()|text()" />
			<xsl:apply-templates select="//parameter[(name/text()=current()/@data-parametername)]/type" mode="label"/>
			<xsl:apply-templates select="//parameter[(name/text()=current()/@data-parametername)]/comment" mode="ajaxLink"/>          
			<xsl:apply-templates select="//parameter[(name/text()=current()/@data-parametername)]/comment" mode="ajaxTargetJob"/>
		</xsl:element>
	</xsl:template>	

	<xsl:template match="example" mode="ajaxLink">
		<a href="#{generate-id(..)}::example" class="exampleLink" title="click to prefill with an example">[use example data]</a>		
		<xsl:if test="not(../comment)">
			<a href="#{generate-id(..)}::comment" class="blindLink commentToggle" title="click to expand/collapse contextual help">?</a>		
		</xsl:if>
	</xsl:template>	
	
	<xsl:template match="example" mode="ajaxTarget">
		<div class="example" id="{generate-id(..)}::example" data-forparameter="{../name/text()}" style="display:none">
			<pre><xsl:apply-templates select="node()" mode="escape"/></pre>
		</div>
		<xsl:if test="not(../comment)">
			<div id="{generate-id(..)}::comment" class="commentText" style="display:none" mode="ajaxTarget">
				<xsl:apply-templates select="../example" mode="exampleInclude"/>
			</div>			
		</xsl:if>
	</xsl:template>	

	<xsl:template match="example" mode="exampleInclude">
		<div class="example" id="{generate-id(..)}::example" data-forparameter="{../name/text()}">Example data <i>(click on <a>[use example data]</a> to load)</i>:
			<pre><xsl:apply-templates select="node()" mode="escape"/></pre>
		</div>
	</xsl:template> 
<!--
    XML escaping code, shamelessly copied and adapted from http://stackoverflow.com/questions/1162352/converting-xml-to-escaped-text-in-xslt
-->
    <xsl:template match="*" mode="escape">
        <!-- Begin opening tag -->
        <xsl:text>&lt;</xsl:text>
        <xsl:value-of select="name()"/>
        <!-- Namespaces -->
        <xsl:for-each select="namespace::*">
            <xsl:if test="name() != 'xml'">
                <xsl:text> xmlns</xsl:text>
                <xsl:text>:</xsl:text>
                <xsl:value-of select="name()"/>
                <xsl:text>='</xsl:text>
                <xsl:call-template name="escape-xml">
                    <xsl:with-param name="text" select="."/>
                </xsl:call-template>
                <xsl:text>'</xsl:text>
            </xsl:if>
        </xsl:for-each>

        <!-- Attributes -->
        <xsl:for-each select="@*">
            <xsl:text> </xsl:text>
            <xsl:value-of select="name()"/>
            <xsl:text>='</xsl:text>
            <xsl:call-template name="escape-xml">
                <xsl:with-param name="text" select="."/>
            </xsl:call-template>
            <xsl:text>'</xsl:text>
        </xsl:for-each>

        <!-- End opening tag -->
        <xsl:text>&gt;</xsl:text>

        <!-- Content (child elements, text nodes, and PIs) -->
        <xsl:apply-templates select="node()" mode="escape" />

        <!-- Closing tag -->
        <xsl:text>&lt;/</xsl:text>
        <xsl:value-of select="name()"/>
        <xsl:text>&gt;</xsl:text>
    </xsl:template>

    <xsl:template match="text()" mode="escape">
        <xsl:call-template name="escape-xml">
            <xsl:with-param name="text" select="."/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="processing-instruction()" mode="escape">
        <xsl:text>&lt;?</xsl:text>
        <xsl:value-of select="name()"/>
        <xsl:text> </xsl:text>
        <xsl:call-template name="escape-xml">
            <xsl:with-param name="text" select="."/>
        </xsl:call-template>
        <xsl:text>?&gt;</xsl:text>
    </xsl:template>

    <xsl:template name="escape-xml">
        <xsl:param name="text"/>
        <xsl:if test="$text != ''">
            <xsl:variable name="head" select="substring($text, 1, 1)"/>
            <xsl:variable name="tail" select="substring($text, 2)"/>
            <xsl:choose>
                <xsl:when test="$head = '&amp;'">&amp;amp;</xsl:when>
                <xsl:when test="$head = '&lt;'">&amp;lt;</xsl:when>
                <xsl:when test="$head = '&gt;'">&amp;gt;</xsl:when>
                <xsl:when test="$head = '&quot;'">&amp;quot;</xsl:when>
                <xsl:when test="$head = &quot;&apos;&quot;">&amp;apos;</xsl:when>
                <xsl:otherwise><xsl:value-of select="$head"/></xsl:otherwise>
            </xsl:choose>
            <xsl:call-template name="escape-xml">
                <xsl:with-param name="text" select="$tail"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>
