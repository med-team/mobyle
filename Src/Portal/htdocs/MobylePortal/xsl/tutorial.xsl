<?xml version="1.0" encoding="utf-8"?>
<!-- 
	tutorial.xsl stylesheet
	Authors: Hervé Ménager
-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml">

	<!-- this uri is the identifier of the tutorial xml, i.e., the url of the xml definition on its execution server --> 
	<xsl:param name="programUri" />	

	<!-- this uri is the identifier of the tutorial xml, i.e., the url of the xml definition on its execution server --> 
	<xsl:param name="programPID" /> 

	<xsl:variable name="server" select="substring-before($programPID,'.')" />
	
	<xsl:param name="codebase">/data/</xsl:param>
	
	<xsl:include href="ident.xsl" />

	<xsl:template match="/|comment()|processing-instruction()">
		<xsl:copy>
			<!-- go process children (applies to root node only) -->
			<xsl:apply-templates/>
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="*">
		<xsl:element name="{local-name()}">
			<!-- go process attributes and children -->
			<xsl:apply-templates select="@*|node()"/>
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="@*">
		<xsl:choose>
			<xsl:when test="contains(.,'tutorial-codebase')">
				<xsl:attribute name="{name()}" namespace="{namespace-uri()}"><xsl:value-of select="substring-before(.,'tutorial-codebase')"/><xsl:value-of select="concat($codebase,'services/servers/local/tutorials/',/*/head/name/text())"/><xsl:value-of select="substring-after(.,'tutorial-codebase')"/></xsl:attribute>        
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="{local-name()}">
					<xsl:value-of select="."/>
				</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	

	<xsl:template match="/">
			<table class="header">
				<tr>
					<td style="width: 60%">
						<xsl:apply-templates select="*/head" mode="serviceHeader"/>						
					</td>
				</tr>
			</table>
			<xsl:apply-templates select="*/head/doc/comment" mode="ajaxTarget"/>
			<xsl:apply-templates select="*/flow"/>
			<xsl:apply-templates select="*/head/interface[@type='tutorial']/*" />
			<xsl:apply-templates select="*/head" mode="serviceFooter"/>
	</xsl:template>

</xsl:stylesheet>
