<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- 
	localize.xsl stylesheet
	Authors: Herv� M�nager
-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xhtml="http://www.w3.org/1999/xhtml"  xmlns:mb="http://www.mobyle.fr">

	<xsl:namespace-alias stylesheet-prefix="mb" result-prefix="#default"/>	

	<xsl:param name="localCategoriesUrl" />
	
	<xsl:variable name="localCategories" select="document($localCategoriesUrl)"/>

	<xsl:include href="ident.xsl" />	

	<xsl:template match="head|package">
		<xsl:variable name="level">
			<xsl:if test="local-name()='head'">program</xsl:if>
			<xsl:if test="local-name()='package'">package</xsl:if>			
		</xsl:variable>
		<xsl:element name="{local-name()}">
		<xsl:apply-templates select="@*|node()[local-name()!='category']|text()" />		
		<xsl:if test="$localCategories//category[@name=current()/name/text() and @class=$level]">
			<xsl:apply-templates select="$localCategories//category[@name=current()/name/text() and @class=$level]" />
		</xsl:if>
			<xsl:if test="not($localCategories//category[@name=current()/name/text() and @class=$level])">
			<xsl:apply-templates select="category" />
		</xsl:if>
		</xsl:element>
	</xsl:template>

	<!-- remove localize.xml category element attributes so that the resulting file validates -->
	<xsl:template match="category/@*"></xsl:template>

</xsl:stylesheet>