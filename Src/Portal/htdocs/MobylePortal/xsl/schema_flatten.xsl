<?xml version="1.0" encoding="utf-8"?>
<!-- 
	ident.xsl stylesheet
	Authors: Hervé Ménager
-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:rng="http://relaxng.org/ns/structure/1.0" >
	<!-- This stylesheet contains the XSL Identity Transform used by many other stylesheets -->

	<xsl:template match="@*|node()|text()" priority="-1">
		<xsl:call-template name="nodeCopy" />
	</xsl:template>

	<xsl:template name="nodeCopy">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()|text()" />
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="rng:include">
		<xsl:copy-of select="document(@href)/rng:grammar/*"/>
	</xsl:template>

</xsl:stylesheet>