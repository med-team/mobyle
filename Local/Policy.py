import sys
import os.path


from logging  import getLogger
p_log = getLogger('Mobyle.Policy')



def queue( queueName ):
    """
    @return: the name of the queue to be used to execute the job
    @rtype: string
    """
    return queueName



#if you want to defined a local policy to check user
# you must put you code here 
def emailCheck( **args ):
     """
     check if the email according to the local rules.
     @return:
      - Mobyle.Net.EmailAddress.VALID    if the email is valid
      - Mobyle.Net.EmailAddress.INVALID  if the email is rejected
      - Mobyle.Net.EmailAddress.CONTINUE to continue futher the email validation process
     """
     import Mobyle.Net

     return Mobyle.Net.EmailAddress.CONTINUE
 


def authenticate( login , passwd ):
    """
    Mobyle administartor can put authentification code here. this function must return either 
     - Mobyle.AuthenticatedSession.AuthenticatedSession.CONTINUE: 
                the method does not autentified this login/passwd. The fall back method must be applied.
     - Mobyle.AuthenticatedSession.AuthenticatedSession.VALID:    
               the login/password has been authenticated.
     - Mobyle.AuthenticatedSession.AuthenticatedSession.REJECT:    
              the login/password is not allow to continue.
    """
    import Mobyle.AuthenticatedSession
    
    return Mobyle.AuthenticatedSession.AuthenticatedSession.CONTINUE

