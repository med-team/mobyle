########################################################################################
#                                                                                      #
#   Author: Bertrand Neron,                                                            #
#   Organization:'Biological Software and Databases' Group, Institut Pasteur, Paris.   #  
#   Distributed under GPLv2 Licence. Please refer to the COPYING.LIB document.         #
#                                                                                      #
########################################################################################

import logging 
_log = logging.getLogger(__name__)
from Mobyle.MobyleError import MobyleError
from ExecutionConfig import ExecutionConfig

class SGEConfig( ExecutionConfig ):
    
    def __init__( self , root = None , cell = None ):
        if root is None or cell is None:
            msg = "root and cell must be specified for SGEConfig instanciation in Config.EXECUTION_SYSTEM_ALIAS"
            _log.critical( msg )
            raise MobyleError( msg )
        
        super( SGEConfig , self ).__init__()
        self.root = root
        self.cell = cell
        
    