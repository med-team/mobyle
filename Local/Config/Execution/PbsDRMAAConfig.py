########################################################################################
#                                                                                      #
#   Author: Bertrand Neron,                                                            #
#   Organization:'Biological Software and Databases' Group, Institut Pasteur, Paris.   #  
#   Distributed under GPLv2 Licence. Please refer to the COPYING.LIB document.         #
#                                                                                      #
########################################################################################

import logging 
_log = logging.getLogger(__name__)

from DRMAAConfig import DRMAAConfig

class PbsDRMAAConfig( DRMAAConfig ):
    
        def __init__( self , drmaa_library_path , server_name , nativeSpecification = '' ):
            super( PbsDRMAAConfig , self ).__init__( drmaa_library_path , nativeSpecification = nativeSpecification)
            self.contactString = server_name
            
                        
            
        