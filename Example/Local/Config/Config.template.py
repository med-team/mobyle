import os , os.path
# this a python module the syntax used is the python syntax


############################################
#                                          #
#            Mandatory values              #
#                                          #
############################################


# the root url of mobyle 
# warning: do NOT include the port number if it is 80, it is useless
# and creates problems on the server
ROOT_URL = "http://mydomain.fr:port"

#base project url for mobyle cgis = ROOT_URL + CGI_PREFIX
CGI_PREFIX = 'cgi-bin/mobyle/xxxxx'

#base project url for mobyle htdocs = ROOT_URL + HTDOCS_PREFIX
HTDOCS_PREFIX = 'mobyle/xxxxx'

#########
#       #
# mails #
#       #
#########

#a list of email addresses who will received messages when problems occur
MAINTAINER = [ "name@mydommain.fr" ]

#a list of email addresses where the users could have help on this Mobyle portal
HELP= [ "name2@mydommain.fr" , "name3@mydommain.fr" ]

# local mailhost
MAILHOST= "mailhost.mydommain.fr"

##################
#                #
# queuing system #
#                # 
##################

from Execution import  *

#EXECUTION_SYSTEM_ALIAS = {
#                          'DRMAA_sge'   : SgeDRMAAConfig( '/path/to/sge/libdrmaa.so' , 
#                                                          root = 'path to sge root', 
#                                                          cell = 'cell name' ) ,
#                          'DRMAA_torque': PbsDRMAAConfig( '/path/to/torque-pbs/libdrmaa.so' , 
#                                                          'marygay.sis.pasteur.fr' ),
#                          'SGE'         : SGEConfig( root = '/path/to/sge_root', 
#                                                     cell= 'cell name' ) ,
#                          'SYS'         : SYSConfig() , 
#                          'LSF'         : LsfDRMAAConfig( '/path/to/LSF/libdrmaa.so' ,  
#                                                          lsf_envdir = 'the contained of LSF_ENVDIR' ,
#                                                          lsf_serverdir = 'the contained of LSF_SERVERDIR')
#                          }

EXECUTION_SYSTEM_ALIAS = { 'SYS'         : SYSConfig()  }

from Mobyle.Dispatcher import DefaultDispatcher

#DISPATCHER = DefaultDispatcher( { 
#                                  'service1' : ( EXECUTION_SYSTEM_ALIAS[ 'DRMAA_sge' ]    , 'queue name' ),
#                                  'service2' : ( EXECUTION_SYSTEM_ALIAS[ 'DRMAA_torque' ] , 'queue name' ),
#                                  'service3'  : ( EXECUTION_SYSTEM_ALIAS[ 'LSF' ]         , 'queue name' ),
#                                  'service4' : ( EXECUTION_SYSTEM_ALIAS[ 'SGE' ]          , 'queue name' ),
#                                  'service5'    : ( EXECUTION_SYSTEM_ALIAS[ 'DRMAA_sge' ]    , 'queue name' ),
#                                  'DEFAULT'   : ( EXECUTION_SYSTEM_ALIAS[ 'SYS' ]          , '' )
#                           } )

DISPATCHER = DefaultDispatcher( { 'DEFAULT'   : ( EXECUTION_SYSTEM_ALIAS[ 'SYS' ]  , '' ) } )
#######################
#                     #
#       logging       #
#                     #
#######################

#the directory were will stored the log files
LOGDIR = '/var/log/mobyle/'


####################################################
#                                                  #
#    data format detection and conversion          # 
#                                                  #
####################################################

# for Sequence and Alignment, the HIGHLY recommended squizz converters 
# are available
#DATA_CONVERTER={
#      'Datatype1': [converter_class1(path_to_the_corresponding_converter_program), converter_class2(path)],
#      'Datatype2': [converter_class1(path_to_the_corresponding_converter_program), converter_class2(path)]
#      }
from Mobyle.Converter import *

DATA_CONVERTER={
      'Sequence': [ squizz_sequence('/path/to/squizz') ] ,
      'Alignment': [ squizz_alignment('/path/to/squizz')]
                }


          ######################################################################## 
          ######################################################################## 
          ##                                                                    ##
          ##                           Optionals Values                         ##
          ##                                                                    ##
          ########################################################################
          ########################################################################

#########
#       #
# Debug #
#       #
#########

#used in production
# 0    - the command line is build
#      - the build log is NOT fill
#      - the job is executed

#to test a xml ( python syntax in code , precond ... )
# 1    - the command line is build 
#      - the build log is NOT fill
#      - the job is NOT executed

#to know what's wrong in the xml I wrote.
# 2    - the command line is build
#      - the build log is fill
#      - the job is NOT executed

#to test the xml and the job execution and the results retrieving
# 3    - the command line is build
#      - the build log is fill
#      - the job is executed

DEBUG = 3

#to set a different debug level for a particular service
#PARTICULAR_DEBUG = { 'serviceName' : 3 }


###############
#             #
# Directories #
#             #
###############

## were are the binary corresponding to the services
## a list of string
## each element must be a valid path
## the element order is kept to build the final PATH ( the binary path is add before the canonical PATH )

BINARY_PATH = ["/usr/local/bin"]


#DATABANKS_CONFIG = {
#    'wgs':{
#     'dataType' : 'Sequence', 
#     'bioTypes' : ['Nucleic'],    
#     'label'    : 'Genbank - Whole Genome Shotgun',
#     'command': ['golden', '%(db)s:%(id)s']
#    },
#    'pdb':{
#     'dataType':'3DStructure', 
#     'bioTypes':['Protein'], 
#     'label': 'Protein Data Bank', 
#     'command': [ "/path/to/PDBGet.py", "%(id)s" ]
#    }
#    }

DATABANKS_CONFIG = {}

#####################
#
#   Statistics
#
#####################

## Google Analytics code - set to use GA
#GACODE = 'XXXXXXXXXX'



######################
#                    #
#   Authentication   #
#                    #
######################

## Enable OpenID
# default value = False

# OPENID = True

#####################
#
#   Welcome page configuration
#
#####################

## news_ex illustrating example feed is in Example/Local directory.
#WELCOME_CONFIG = {'url':'http://localhost:85/portal/news_ex.atom',
#                 'format':'atom'}

#####################
#
#   Portal custom content for header and footer
#
#####################
#
#CUSTOM_PORTAL_HEADER="<h1>myOwnPortalName</h1>"
#
#CUSTOM_PORTAL_FOOTER="<div>insert the name of your sponsors or your legal disclaimers</div>

## to make email optional for all programs, set this to True
## default value = False

#OPT_EMAIL = False

## to have a more control on the mandatory email
## it able to have the general option but
## it could be set to another value for particular service
## example :
## OPT_EMAIL could be set at False ( the email is mandatory )
## but set a True for some very short services.
# the key is the portal id of a service and value is a boolean
# for local services the portal id the server name can be ommited
# eg 'local.golden' == 'golden'

#PARTICULAR_OPT_EMAIL = {'portal_id' : True }

#if a job is longer than EMAIL_DELAY Mobyle send the results by email (if user give it's email)
#its express in second, 0 mean no email (default = 60)
#EMAIL_DELAY = 60

## anonymous session there is 3 available values 
##  'no'      : the anonymous sessions are not allowed
##  'yes'     : the anonymous sessions are allowed, without any verification 
##  'captcha' : the anonymous sessions are allowed, but with a captcha challenge ( default )

#ANONYMOUS_SESSION = "captcha"


## authenticated session there is 3 available values
##  'no'    : the authenticated session are not allowed.
##  'yes'   : the authenticated session are allowed and activated without any restriction.
##  'email' : the authenticated session are allowed but an email confirmation is needed to activate it (default).

#AUTHENTICATED_SESSION = "email"

##############
#            #
#    misc    #
#            #
##############

## the time to consider that a job is long ( in sec default = 60 , min = 10 )
#TIMEOUT = 60

## refresh frequency for user data in the web portal, in seconds
## default: 240

#REFRESH_FREQUENCY = 240

## max size for any file (2 Go )
#this limit is honored only if the EXECUTION_SYSTEM_ALIAS is set to Sys  (SYSConfig)
#if SGE, PBS, ... is used this must be set directly in the drm configuration.
#FILELIMIT = 2147483648

## max size for a session in bytes ( default = 50Mo = 52428800 )
#SESSIONLIMIT = 52428800 

##max size to preview the results 
##if the results size exceed this limit the results appear as a link ( default value = 1048576 = 1Mib )
#PREVIEW_DATA_LIMIT = 1048576

#the number of "similar" jobs a user is allowed to submit at a given time.
#similar means same email, same command line. ( default = 1 )
#0 disable this control, then the user may submit as many "same" jobs he wants.
#if the email is not provided (depend of the configuration) this control is disabled.    

# !! this new directive replace SIMULTANEOUS_JOBS !!
#MAX_SIMILAR_JOB_PER_USER =1


#the number of jobs than a user can submit at a given time
#0 disable this control, then the user may submit as many "same" jobs he wants (default).
#if the email is not provided (depend of the configuration) this control is disabled.
#MAX_JOB_PER_USER = 0



##########
#        #
#  Mail  #
#        #
##########

##from: sender email address ( default = HELP )
#SENDER = "myname-noreply@mydomain.fr"

##set this True if you don't want results to be sent by email.
##This does not make email optional
#DONT_EMAIL_RESULTS = False

## max size for results by email ( in bytes default 2 Mib )
#MAXMAILSIZE = 2097152

## how long should results be available on the server ( in days default = 10)?
#RESULT_REMAIN = 10


## if you want to resolve the domain name of the user email
## and if it has a mail exchanger field
## (to avoid fake user email address)
## by default DNS_RESOLVER = False
## if DNS_RESOLVER = True dnspython must be installed

#DNS_RESOLVER = False



################
#              #
#   logging    #
#              #
################

## to monitored the elapsed time per job ( default = False )
#ACCOUNTING = True

#######################
#                     #
# disabling services  #
#                     #
#######################

## some times you need to disable the portal for maintaining operation etc...
## if DISABLE_ALL is True no new job could be submit, but the running job keep running

#DISABLE_ALL = False

## To disable specifically one service (program or workflow) from any portal, you can append it in  DISABLED_SERVICES.
## joker can be used, so it's easy to disable all services from a given portal.
## this portal is call 'local'
## to re-enable services just toggle DISABLE_ALL to False or remove it from 
## the DISABLED_SERVICES list
## example:
##DISABLED_SERVICES = [ 'portal1.service1' , # disable the service1 from the imported portal1 (as defined in PORTALS )
##                      'portal2.*' ,        # disable all services from the imported portal2
##                      'local.clustalw*'    # disable all services begining by clustalw (clutalw-multialign ,clustalw-sequence , clustalw-profile ) from this server.
##                     ] 
## By default all services are enabled

#DISABLED_SERVICES = [] 


################################
#                              #
# restriction services access  #
#                              #
################################

## by default all the programs available are usable by all         
## users who can access your web server. but sometimes,due to      
## some license restrictions etc... , you need to restrict the     
## accessibility of some programs to some users.                    
## to do that use the AUTHORIZED_SERVICES                          
## The filtering is based on the ip of the requester.              
## AUTHORIZED_SERVICES is a dictionary with the service names of   
## programs to restrict as keys and the list of ip which can          
## access these programs as values                                 
##                                                                 
## AUTHORIZED_SERVICES = { serviceURL :[ ip , ip mask ] }         
##
## the ip address which can use the service
##
## AUTHORIZED_SERVICES = 'http://myMobyle.mydomain.fr/data/programs/toppred.xml' : [ '125.234.60.18' , # only the machine with this ip could access to toppred
##                                                                                   '125.234.60.*'  , # all the machines in subnet could access to toppred
##                                                                                   '125.234.*.15   , # all the machine 
##                                                                                 ] 
##
## if there is no entry for a service it's mean that
## every body can access to this service


#AUTHORIZED_SERVICES = {}


################################
#                              #
#     Services Management      #
#                              #
################################

##################################################################################
#                                                                                #
# Local program publishing section                                               #
#                                                                                #
# This configuration file is used to deploy the xml services descriptions on     # 
# the Mobyle web part from the Local/Services/* and /Srevices/* directories.     # 
# all the xml from Local/Services/*/* are deployed                               #
# the xml from Services/*/* are filtered following the rules below               #  
# (for more explanations see INSTALL notes and Tools/README)                     #
#                                                                                #
# (if this file doesn't exist all the programs from Programs will be published)  # 
#                                                                                #
##################################################################################

#the list of programs to deploy is established by filtering the Local/Services/ repository and Services repository
#with the LOCAL_DEPLOY_INCLUDE following the LOCAL_DEPLOY_EXCLUDE.
#each value corresponds to a unix mask to filter a directory, so the unix joker * can be used.

#LOCAL_DEPLOY_INCLUDE = { 'programs' : [ 'blast*' ] ,             #all programs definitions begining by blast will be deployed
#                          'workflows': [ '*' ] ,                 #all workflows will be deployed
#                          'viewers'  : [ 'jalview' , 'varna' ] , #only jalview and varna will be deployed
#                          }

#LOCAL_DEPLOY_EXCLUDE = { 'programs' : [ 'blast2mydb' ] , #blast2mydb will be removed from the list of programs to deploy
#                          'workflows': [ '' ] ,
#                          'viewers'  : [ '' ] ,
#                          }

LOCAL_DEPLOY_INCLUDE = { 'programs' : [ '*' ] ,
                          'workflows': [ '*' ] ,
                          'viewers'  : [ '*' ] ,
                          }

LOCAL_DEPLOY_EXCLUDE = { 'programs' : [ '' ] ,
                          'workflows': [ '' ] ,
                          'viewers'  : [ '' ] ,
                          }


######################
#                    #
#    Grid aspects    #
#                    #
######################
# PORTAL_NAME is the name of the portal as it is known to others in a MobyleNet network
#PORTAL_NAME = "my_server"

# SIMPLE_FORMS activates the display of simple forms by default
# inactive by default
#SIMPLE_FORMS = False

#PORTALS={'portal1': {
#            'url': 'http://otherdomain.fr:port/cgi-bin/MobylePortal',
#            'help' : 'help@otherdomain.fr',
#            'repository': 'http://otherdomain.fr:port/',
#            'services': { 'programs' : ['clustalw-multialign'],
#                          'workflows': [ 'wokflow1_of_portal1']
#            }
#        }
PORTALS={}

#EXPORTED_SERVICES = [ 'golden', 'abiview' ]


