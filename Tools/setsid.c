
#include <unistd.h>

int main(int argc, char **argv) {

  /* Make this process a group leader */
  (void)setsid();

  /* Execute the real command */
  execvp(argv[1], argv+1);

  return 1; }
