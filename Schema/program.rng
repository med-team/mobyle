<?xml version="1.0" encoding="UTF-8"?>
<grammar xmlns:a="http://relaxng.org/ns/compatibility/annotations/1.0" xmlns="http://relaxng.org/ns/structure/1.0" datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes">
  <include href="common.rng"/>
  <start combine="choice">
    <ref name="Program"/>
  </start>
  <define name="Program">
    <element name="program">
      <ref name="Service"/>
    </element>
  </define>
  <define name="InvocationHead" combine="interleave">
    <interleave>
      <optional>
        <interleave>
          <optional>
            <element name="command">
              <a:documentation>executable name</a:documentation>
              <interleave>
                <optional>
                  <attribute name="path">
                    <a:documentation>custom path	</a:documentation>
                  </attribute>
                </optional>
                <text/>
              </interleave>
            </element>
          </optional>
          <zeroOrMore>
            <element name="env">
              <a:documentation>environment variable for program invocation</a:documentation>
              <interleave>
                <attribute name="name">
                  <a:documentation>name</a:documentation>
                </attribute>
                <text>
                  <a:documentation>value</a:documentation>
                </text>
              </interleave>
            </element>
          </zeroOrMore>
        </interleave>
      </optional>
      <optional>
        <!-- displayed even before the program completes -->
        <element name="progressReport">
          <a:documentation>progress report output for the program</a:documentation>
          <interleave>
            <optional>
              <attribute name="prompt">
                <a:documentation>prompt to "label" the progress report</a:documentation>
              </attribute>
            </optional>
            <text/>
          </interleave>
        </element>
      </optional>
    </interleave>
  </define>
  <define name="InvocationParagraph" combine="interleave">
    <optional>
      <ref name="Argpos"/>
    </optional>
  </define>
  <define name="InvocationParameterElements" combine="interleave">
    <interleave>
      <choice>
        <optional>
          <!-- iscommand : if true& this parameter specify the line of command to run the program  used when the command line is more complicated. -->
          <attribute name="iscommand">
            <a:documentation>command parameter</a:documentation>
            <data type="boolean"/>
          </attribute>
        </optional>
        <optional>
          <element name="paramfile">
            <a:documentation>parameter file</a:documentation>
            <text/>
          </element>
        </optional>
      </choice>
      <optional>
        <ref name="Format">
          <a:documentation>command line chunk evaluation code </a:documentation>
        </ref>
      </optional>
      <optional>
        <ref name="Argpos">
          <a:documentation>command line chunk position index</a:documentation>
        </ref>
      </optional>
      <optional>
        <!--
          this element is relevant only for output parameters, it is used to find the files in which the results are stored
          each code must have ONLY ONE file unix mask 
        -->
        <element name="filenames">
          <a:documentation>result file(s) mask(s) </a:documentation>
          <oneOrMore>
            <ref name="Code"/>
          </oneOrMore>
        </element>
      </optional>
    </interleave>
  </define>
  <define name="Argpos">
    <!--
      in parameter: specifies the position of this parameter on the command line
      in paragraph: specifies the position on the command line for all parameters of this paragraph
      by convention the argpos of command must be 0. if we want args before command we could get negative argpos.
    -->
    <element name="argpos">
      <a:documentation>command line chunk position index </a:documentation>
      <data type="integer"/>
    </element>
  </define>
  <define name="Format">
    <!-- a code which will be evaluated to form the command line -->
    <element name="format">
      <a:documentation>command line chunk construction code</a:documentation>
      <oneOrMore>
        <ref name="Code"/>
      </oneOrMore>
    </element>
  </define>
  <define name="InterfaceType" combine="interleave">
    <choice>
      <value>form</value>
      <value>job_input</value>
      <value>job_output</value>
    </choice>
  </define>
</grammar>
