datatypes xsd = "http://www.w3.org/2001/XMLSchema-datatypes" 
namespace xhtml = "http://www.w3.org/1999/xhtml"
namespace svg = "http://www.w3.org/2000/svg"

#---------------------------------------------------------------#
#                     Mobyle Generic Service Elements           #
#---------------------------------------------------------------#

Service = (
    element head { Head }&
	## Parameters list
	Parameters&
	Implementation
)

Head = (
		## name
		Name& 
		## package
		Package?&
		## version
		element version { text }?&
		## information
		element doc {
			## title
			element title { text }&
			## description
			element description { (Text*|anyXHTMLorSVG) }&
			## authors
			element authors { anyXHTMLorSVG }?&
			## reference
			element reference {
				## Document Object Identifier
				attribute doi {xsd:anyURI}?& 
				## URL
				attribute url {xsd:anyURI}?& 
				## formatted reference (text or HTML code)
				anyXHTMLorSVG
				}*&
			## documentation link(s)
			element doclink { xsd:anyURI }*&
			## download link(s)
			element sourcelink { (xsd:anyURI | xsd:string) }*&
			## homepage link(s)
			element homepagelink { xsd:anyURI }*&
			## additional comments
			Comment?
			}&
		## classification category
		element category { text }*&
		## EDAM classification category
		element edam_cat { attribute ref { xsd:string }, attribute label { xsd:string } }*&
		## bioMOBY service category
		element biomobyCategory { text }?& #this element stores the biomoby serviceType property value.
		## invocation elements
        InvocationHead&
        Interface*
    )

Name = element name {xsd:string}

Comment = element comment { (Text*|anyXHTMLorSVG) }

## package element defines elements common to all the interfaces of a package
Package = element package {
	Head
}

Parameters = element parameters {
	(Parameter | Paragraph)*
	}

Parameter = element parameter {
(
    ## technical ID
    # id so far is added to facilitate workflow developments
    attribute id { xsd:string }?&
	## program main input
	# ismaininput : if true the parameter is a primary article of the corresponding biomoby service (as used in PlayMoby)
	attribute ismaininput { xsd:boolean }?&
	## mandatory parameter
	# ismandatory : if true this parameter should be specified by the user 
	attribute ismandatory { xsd:boolean }?&
	## hidden parameter
	attribute ishidden { xsd:boolean }?&
	## show in simple forms	
	# issimple : specify if this parameter will be displayed only in the simple web form.
	attribute issimple { xsd:boolean }?&
	(
	# isout and isstdout are mutually exclusive
	## output parameter
	# isout : if true this parameter is produced by this service 
	attribute isout { xsd:boolean }? 
	| 
	## standard output parameter
	# isstdout : if true this parameter is the standart output of the service. Only one parameter can be isstdout="1" per service
	attribute isstdout { xsd:boolean }?
	)&
 	## parameter name
	Name&
	## parameter prompt
	Prompt* &
	## parameter type
	element type {
		## parameter biotype
		# the biological type of the parameter DNA & Protein ...
		# the biotypes list represents the list of accepted 'types'
		# for input parameters and the list of 'types' that an 
		# output may belong to
		element biotype { text }*&
		## parameter datatype
		# type of the data Sequence & Structure & Matrix ...
		element datatype { 
			## datatype class
			element class { text }&
			## datatype superclass
			#  superclass or class if class does not exist
			#  is a Mobyle parameter Class defined in Src/Mobyle/* or in Local/CustomClasses/*
			element superclass { text }?
			}&
		## accepted data formats
        # acceptedDataFormats is a deprecated tag, and will be removed in a future version - please use only non-nested dataFormat tags
		DataFormat*&		
		## parameter cardinality
		# the cardinality of the datatype &a number or a couple of value &  int&int or int&n
		# biotype=Protein datatype=( class = Sequence ) & format=Fasta & card=1  means
		# the user must provide 1 and only one Protein Sequence in Fasta format to fill this input parameter 
		# biotype=Protein datatype=( class = Sequence ) & format=Fasta & card=1.n  means
		# the user must provide 1 or more  Protein Sequence in Fasta format to fill this input parameter
		element card { text }?&
		## bioMOBY typing
                # the biomoby typing accepts only one format for a biomoby service
		# however many programs accept different formats for a given parameter
		# thus multiple biomoby datatypes can be specified, and a single mobyle
		# program can be seen as multiple biomoby services.
		# the name of the object in the bioMoby ontology. it's implied
		# that it's a primary and if not specify the parameter is a secondary in biomoby
		element biomoby { 
				## bioMOBY datatype
				element datatype {text}*&
				## bioMOBY namespace
				element namespace {text}*
			}?
		}&
	## parameter relevancy condition
	Precond?&
	## parameter default value(s)
	# the default value or list of values (clustalw) for the parameter
	# the vdef element is mandatory if the parameter has a vlist or a flist.
    ## EDAM type
	element edam_type { attribute ref { xsd:string }, attribute label { xsd:string } }*&
	element vdef { Value+ }?&
	(
	  	## list of authorized values
		# a list of the available values for this parameter?
		element vlist { 
			element velem { 
				ListElem			
				}+
			}?
		|
		## list of authorized computed values 
		# flist permit to associate a code with a value which is associate to a label in interface
		# you could associate different codes with different proglangs for one value
		element flist {
			element felem { 
				ListElem&
				Code+ }*
			}?
	)&
	## multiple values separator
	# the character used to split the elements of a vlist for the MultipleChoice?
	element separator { text }?&
	## parameter value validation code
	# associated a message to a code& the code permit to
	# specify the valids values for a parameter*
	element ctrl { Message& Code+ }*&
	## authorized value range
	element scale { 
		## minimum value
		element min { (Value | Code+) }&
		## maximum value 
		element max { (Value | Code+) }&
		## increment 
		element inc { text }? 
		}?&
	## comments
	Comment?&
	## example value
	element example { anyTextOrXML }?&
	## invocation part
	InvocationParameterElements?&
	## custom parameter interface
	Interface*
)|
# this is an automatically generated parameter element needed for the interface generation
## auto-generated element
(
    (
    ## auto-generated standard error
    attribute isstderr { xsd:boolean }? 
    | 
    ## auto-generated standard output parameter
    attribute isstdout { xsd:boolean }?
    )&
    Interface*
)
	
}

ListElem = 
	(
	## undefined value
	# the undef attribute specifies if an element is an alias for a non specified value.
	# this links a label to the undefined value in the selectbox of the web form.
	attribute undef { xsd:boolean }?& 
	## value
	Value& 
	## label
	Label
	)

DataFormat =
	## data format 
	#  the format of the data 
	# if datatype == Sequence  format could have this values :
	# IG & GENBANK & NBRF & EMBL & GCG & DNASTRIDER & FASTA & RAW & 
	# PIR & XML & SWISSPROT & GDE  &
	element dataFormat {
	            ## force data reformatting
		    	attribute force { xsd:boolean }?&
		    	DynamicFunction }

DynamicFunction = 
    (
    text | 
    element test {
        attribute param { text },
        (attribute eq { text }|
        attribute ne { text }|
        attribute lt { text }|
        attribute gt { text }|
        attribute le { text }|
        attribute ge { text }),
        DynamicFunction
    } |
    element ref {attribute param {text}}
    )*

Prompt =
	## prompt 
	element prompt { Lang?& text }

Lang = 
	## language
	#  lang as in RFC-1766 (e.g.& en-US& fr& fr-FR& etc.)
	attribute lang { xsd:language }

Label = 
	## label
	# the text associated to a value which will be display on the form
	element label { text }

Message =
	## message 
	# a message displayed when the control fail
	element message { Text+ }

Precond =
	## relevancy condition 
	# in parameter : the parameter is linked to a condition
	# ex : this parameter could be used only if an other parameter has been specified
	element precond { Code+ }

Value = 
	## value
	element value { text }

Code = 
	## code snippet in a programming language
	element code {
		## programming language (e.g., python, perl, etc.)
		attribute proglang {text},
		non-empty-string
		}

## text element
Text = 
	element text {
		Lang&
		text 
		}
## HTML element
anyXHTMLorSVG = (
	element xhtml:* {
   (attribute * { text }
    | text
    | anyXHTMLorSVG)*
}|text|element svg:* {
   (attribute * { text }
    | text
    | anyXHTMLorSVG)*
})*

anyTextOrXML = (
element * {
   (attribute * { text }
    | text
    | anyTextOrXML)*
}|text
)

## paragraph
Paragraph = element paragraph {
	Name&
	Prompt*&
	Precond*&
	Comment?&
	Parameters& 
  InvocationParagraph&
	## custom paragraph layout/interface
	(Layout|Interface*)?
}

## box-model layout
Layout = element layout {
	(Hbox | Vbox)+
}

## horizontal box
Hbox = element hbox {
	(Box | Layout) + 
}

## vertical box
Vbox = element vbox {
	(Box | Layout) + 
}

## box
Box = element box { text }

## custom interface specification
Interface = element interface {
	## override default Choice parameter display
	(attribute field {"select"|"radio"}|
    (attribute type {InterfaceType}&
     attribute generated {"true"}?&
	## override HTML display code
	anyXHTMLorSVG))
}


Email = element email {xsd:token {pattern = "[a-z0-9\-\._]+@([a-z0-9\-]+\.)+([a-z]){2,4}"}}

non-empty-string = xsd:token { minLength = "1" }

Implementation = empty

InvocationHead = empty

InvocationParameterElements = empty

InvocationParagraph = empty

InterfaceType = empty
