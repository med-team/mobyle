<?xml version="1.0" encoding="UTF-8"?>
<grammar xmlns:svg="http://www.w3.org/2000/svg" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:a="http://relaxng.org/ns/compatibility/annotations/1.0" xmlns="http://relaxng.org/ns/structure/1.0" datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes">
  <!--
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -#
                        Mobyle Generic Service Elements           #
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -#
  -->
  <define name="Service">
    <interleave>
      <element name="head">
        <ref name="Head"/>
      </element>
      <ref name="Parameters">
        <a:documentation>Parameters list</a:documentation>
      </ref>
      <ref name="Implementation"/>
    </interleave>
  </define>
  <define name="Head">
    <interleave>
      <ref name="Name">
        <a:documentation>name</a:documentation>
      </ref>
      <optional>
        <ref name="Package">
          <a:documentation>package</a:documentation>
        </ref>
      </optional>
      <optional>
        <element name="version">
          <a:documentation>version</a:documentation>
          <text/>
        </element>
      </optional>
      <element name="doc">
        <a:documentation>information</a:documentation>
        <interleave>
          <element name="title">
            <a:documentation>title</a:documentation>
            <text/>
          </element>
          <element name="description">
            <a:documentation>description</a:documentation>
            <choice>
              <zeroOrMore>
                <ref name="Text"/>
              </zeroOrMore>
              <ref name="anyXHTMLorSVG"/>
            </choice>
          </element>
          <optional>
            <element name="authors">
              <a:documentation>authors</a:documentation>
              <ref name="anyXHTMLorSVG"/>
            </element>
          </optional>
          <zeroOrMore>
            <element name="reference">
              <a:documentation>reference</a:documentation>
              <interleave>
                <optional>
                  <attribute name="doi">
                    <a:documentation>Document Object Identifier</a:documentation>
                    <data type="anyURI"/>
                  </attribute>
                </optional>
                <optional>
                  <attribute name="url">
                    <a:documentation>URL</a:documentation>
                    <data type="anyURI"/>
                  </attribute>
                </optional>
                <ref name="anyXHTMLorSVG">
                  <a:documentation>formatted reference (text or HTML code)</a:documentation>
                </ref>
              </interleave>
            </element>
          </zeroOrMore>
          <zeroOrMore>
            <element name="doclink">
              <a:documentation>documentation link(s)</a:documentation>
              <data type="anyURI"/>
            </element>
          </zeroOrMore>
          <zeroOrMore>
            <element name="sourcelink">
              <a:documentation>download link(s)</a:documentation>
              <choice>
                <data type="anyURI"/>
                <data type="string"/>
              </choice>
            </element>
          </zeroOrMore>
          <zeroOrMore>
            <element name="homepagelink">
              <a:documentation>homepage link(s)</a:documentation>
              <data type="anyURI"/>
            </element>
          </zeroOrMore>
          <optional>
            <ref name="Comment">
              <a:documentation>additional comments</a:documentation>
            </ref>
          </optional>
        </interleave>
      </element>
      <zeroOrMore>
        <element name="category">
          <a:documentation>classification category</a:documentation>
          <text/>
        </element>
      </zeroOrMore>
      <zeroOrMore>
        <element name="edam_cat">
          <a:documentation>EDAM classification category</a:documentation>
          <attribute name="ref">
            <data type="string"/>
          </attribute>
          <attribute name="label">
            <data type="string"/>
          </attribute>
        </element>
      </zeroOrMore>
      <optional>
        <element name="biomobyCategory">
          <a:documentation>bioMOBY service category</a:documentation>
          <text/>
        </element>
      </optional>
      <!-- this element stores the biomoby serviceType property value. -->
      <ref name="InvocationHead">
        <a:documentation>invocation elements</a:documentation>
      </ref>
      <zeroOrMore>
        <ref name="Interface"/>
      </zeroOrMore>
    </interleave>
  </define>
  <define name="Name">
    <element name="name">
      <data type="string"/>
    </element>
  </define>
  <define name="Comment">
    <element name="comment">
      <choice>
        <zeroOrMore>
          <ref name="Text"/>
        </zeroOrMore>
        <ref name="anyXHTMLorSVG"/>
      </choice>
    </element>
  </define>
  <define name="Package">
    <a:documentation>package element defines elements common to all the interfaces of a package</a:documentation>
    <element name="package">
      <ref name="Head"/>
    </element>
  </define>
  <define name="Parameters">
    <element name="parameters">
      <zeroOrMore>
        <choice>
          <ref name="Parameter"/>
          <ref name="Paragraph"/>
        </choice>
      </zeroOrMore>
    </element>
  </define>
  <define name="Parameter">
    <element name="parameter">
      <choice>
        <interleave>
          <optional>
            <!-- id so far is added to facilitate workflow developments -->
            <attribute name="id">
              <a:documentation>technical ID</a:documentation>
              <data type="string"/>
            </attribute>
          </optional>
          <optional>
            <!-- ismaininput : if true the parameter is a primary article of the corresponding biomoby service (as used in PlayMoby) -->
            <attribute name="ismaininput">
              <a:documentation>program main input</a:documentation>
              <data type="boolean"/>
            </attribute>
          </optional>
          <optional>
            <!-- ismandatory : if true this parameter should be specified by the user -->
            <attribute name="ismandatory">
              <a:documentation>mandatory parameter</a:documentation>
              <data type="boolean"/>
            </attribute>
          </optional>
          <optional>
            <attribute name="ishidden">
              <a:documentation>hidden parameter</a:documentation>
              <data type="boolean"/>
            </attribute>
          </optional>
          <optional>
            <!-- issimple : specify if this parameter will be displayed only in the simple web form. -->
            <attribute name="issimple">
              <a:documentation>show in simple forms	</a:documentation>
              <data type="boolean"/>
            </attribute>
          </optional>
          <choice>
            <optional>
              <!-- isout and isstdout are mutually exclusive -->
              <!-- isout : if true this parameter is produced by this service -->
              <attribute name="isout">
                <a:documentation>output parameter</a:documentation>
                <data type="boolean"/>
              </attribute>
            </optional>
            <optional>
              <!-- isstdout : if true this parameter is the standart output of the service. Only one parameter can be isstdout="1" per service -->
              <attribute name="isstdout">
                <a:documentation>standard output parameter</a:documentation>
                <data type="boolean"/>
              </attribute>
            </optional>
          </choice>
          <ref name="Name">
            <a:documentation>parameter name</a:documentation>
          </ref>
          <zeroOrMore>
            <ref name="Prompt">
              <a:documentation>parameter prompt</a:documentation>
            </ref>
          </zeroOrMore>
          <element name="type">
            <a:documentation>parameter type</a:documentation>
            <interleave>
              <zeroOrMore>
                <!--
                  the biological type of the parameter DNA & Protein ...
                  the biotypes list represents the list of accepted 'types'
                  for input parameters and the list of 'types' that an 
                  output may belong to
                -->
                <element name="biotype">
                  <a:documentation>parameter biotype</a:documentation>
                  <text/>
                </element>
              </zeroOrMore>
              <!-- type of the data Sequence & Structure & Matrix ... -->
              <element name="datatype">
                <a:documentation>parameter datatype</a:documentation>
                <interleave>
                  <element name="class">
                    <a:documentation>datatype class</a:documentation>
                    <text/>
                  </element>
                  <optional>
                    <!--
                       superclass or class if class does not exist
                       is a Mobyle parameter Class defined in Src/Mobyle/* or in Local/CustomClasses/*
                    -->
                    <element name="superclass">
                      <a:documentation>datatype superclass</a:documentation>
                      <text/>
                    </element>
                  </optional>
                </interleave>
              </element>
              <zeroOrMore>
                <!-- acceptedDataFormats is a deprecated tag, and will be removed in a future version - please use only non-nested dataFormat tags -->
                <ref name="DataFormat">
                  <a:documentation>accepted data formats</a:documentation>
                </ref>
              </zeroOrMore>
              <optional>
                <!--
                  the cardinality of the datatype &a number or a couple of value &  int&int or int&n
                  biotype=Protein datatype=( class = Sequence ) & format=Fasta & card=1  means
                  the user must provide 1 and only one Protein Sequence in Fasta format to fill this input parameter 
                  biotype=Protein datatype=( class = Sequence ) & format=Fasta & card=1.n  means
                  the user must provide 1 or more  Protein Sequence in Fasta format to fill this input parameter
                -->
                <element name="card">
                  <a:documentation>parameter cardinality</a:documentation>
                  <text/>
                </element>
              </optional>
              <optional>
                <!--
                  the biomoby typing accepts only one format for a biomoby service
                  however many programs accept different formats for a given parameter
                  thus multiple biomoby datatypes can be specified, and a single mobyle
                  program can be seen as multiple biomoby services.
                  the name of the object in the bioMoby ontology. it's implied
                  that it's a primary and if not specify the parameter is a secondary in biomoby
                -->
                <element name="biomoby">
                  <a:documentation>bioMOBY typing</a:documentation>
                  <interleave>
                    <zeroOrMore>
                      <element name="datatype">
                        <a:documentation>bioMOBY datatype</a:documentation>
                        <text/>
                      </element>
                    </zeroOrMore>
                    <zeroOrMore>
                      <element name="namespace">
                        <a:documentation>bioMOBY namespace</a:documentation>
                        <text/>
                      </element>
                    </zeroOrMore>
                  </interleave>
                </element>
              </optional>
            </interleave>
          </element>
          <optional>
            <ref name="Precond">
              <a:documentation>parameter relevancy condition</a:documentation>
            </ref>
          </optional>
          <zeroOrMore>
            <element name="edam_type">
              <a:documentation>parameter default value(s)</a:documentation>
              <!--
                the default value or list of values (clustalw) for the parameter
                the vdef element is mandatory if the parameter has a vlist or a flist.
              -->
              <a:documentation>EDAM type</a:documentation>
              <attribute name="ref">
                <data type="string"/>
              </attribute>
              <attribute name="label">
                <data type="string"/>
              </attribute>
            </element>
          </zeroOrMore>
          <optional>
            <element name="vdef">
              <oneOrMore>
                <ref name="Value"/>
              </oneOrMore>
            </element>
          </optional>
          <choice>
            <optional>
              <!-- a list of the available values for this parameter? -->
              <element name="vlist">
                <a:documentation>list of authorized values</a:documentation>
                <oneOrMore>
                  <element name="velem">
                    <ref name="ListElem"/>
                  </element>
                </oneOrMore>
              </element>
            </optional>
            <optional>
              <!--
                flist permit to associate a code with a value which is associate to a label in interface
                you could associate different codes with different proglangs for one value
              -->
              <element name="flist">
                <a:documentation>list of authorized computed values </a:documentation>
                <zeroOrMore>
                  <element name="felem">
                    <interleave>
                      <ref name="ListElem"/>
                      <oneOrMore>
                        <ref name="Code"/>
                      </oneOrMore>
                    </interleave>
                  </element>
                </zeroOrMore>
              </element>
            </optional>
          </choice>
          <optional>
            <!-- the character used to split the elements of a vlist for the MultipleChoice? -->
            <element name="separator">
              <a:documentation>multiple values separator</a:documentation>
              <text/>
            </element>
          </optional>
          <zeroOrMore>
            <!--
              associated a message to a code& the code permit to
              specify the valids values for a parameter*
            -->
            <element name="ctrl">
              <a:documentation>parameter value validation code</a:documentation>
              <interleave>
                <ref name="Message"/>
                <oneOrMore>
                  <ref name="Code"/>
                </oneOrMore>
              </interleave>
            </element>
          </zeroOrMore>
          <optional>
            <element name="scale">
              <a:documentation>authorized value range</a:documentation>
              <interleave>
                <element name="min">
                  <a:documentation>minimum value</a:documentation>
                  <choice>
                    <ref name="Value"/>
                    <oneOrMore>
                      <ref name="Code"/>
                    </oneOrMore>
                  </choice>
                </element>
                <element name="max">
                  <a:documentation>maximum value </a:documentation>
                  <choice>
                    <ref name="Value"/>
                    <oneOrMore>
                      <ref name="Code"/>
                    </oneOrMore>
                  </choice>
                </element>
                <optional>
                  <element name="inc">
                    <a:documentation>increment </a:documentation>
                    <text/>
                  </element>
                </optional>
              </interleave>
            </element>
          </optional>
          <optional>
            <ref name="Comment">
              <a:documentation>comments</a:documentation>
            </ref>
          </optional>
          <optional>
            <element name="example">
              <a:documentation>example value</a:documentation>
              <ref name="anyTextOrXML"/>
            </element>
          </optional>
          <optional>
            <ref name="InvocationParameterElements">
              <a:documentation>invocation part</a:documentation>
            </ref>
          </optional>
          <zeroOrMore>
            <ref name="Interface">
              <a:documentation>custom parameter interface</a:documentation>
            </ref>
          </zeroOrMore>
        </interleave>
        <!-- this is an automatically generated parameter element needed for the interface generation -->
        <interleave>
          <a:documentation>auto-generated element</a:documentation>
          <choice>
            <optional>
              <attribute name="isstderr">
                <a:documentation>auto-generated standard error</a:documentation>
                <data type="boolean"/>
              </attribute>
            </optional>
            <optional>
              <attribute name="isstdout">
                <a:documentation>auto-generated standard output parameter</a:documentation>
                <data type="boolean"/>
              </attribute>
            </optional>
          </choice>
          <zeroOrMore>
            <ref name="Interface"/>
          </zeroOrMore>
        </interleave>
      </choice>
    </element>
  </define>
  <define name="ListElem">
    <interleave>
      <optional>
        <!--
          the undef attribute specifies if an element is an alias for a non specified value.
          this links a label to the undefined value in the selectbox of the web form.
        -->
        <attribute name="undef">
          <a:documentation>undefined value</a:documentation>
          <data type="boolean"/>
        </attribute>
      </optional>
      <ref name="Value">
        <a:documentation>value</a:documentation>
      </ref>
      <ref name="Label">
        <a:documentation>label</a:documentation>
      </ref>
    </interleave>
  </define>
  <define name="DataFormat">
    <!--
       the format of the data 
      if datatype == Sequence  format could have this values :
      IG & GENBANK & NBRF & EMBL & GCG & DNASTRIDER & FASTA & RAW & 
      PIR & XML & SWISSPROT & GDE  &
    -->
    <element name="dataFormat">
      <a:documentation>data format </a:documentation>
      <interleave>
        <optional>
          <attribute name="force">
            <a:documentation>force data reformatting</a:documentation>
            <data type="boolean"/>
          </attribute>
        </optional>
        <ref name="DynamicFunction"/>
      </interleave>
    </element>
  </define>
  <define name="DynamicFunction">
    <zeroOrMore>
      <choice>
        <text/>
        <element name="test">
          <attribute name="param"/>
          <choice>
            <attribute name="eq"/>
            <attribute name="ne"/>
            <attribute name="lt"/>
            <attribute name="gt"/>
            <attribute name="le"/>
            <attribute name="ge"/>
          </choice>
          <ref name="DynamicFunction"/>
        </element>
        <element name="ref">
          <attribute name="param"/>
        </element>
      </choice>
    </zeroOrMore>
  </define>
  <define name="Prompt">
    <element name="prompt">
      <a:documentation>prompt </a:documentation>
      <interleave>
        <optional>
          <ref name="Lang"/>
        </optional>
        <text/>
      </interleave>
    </element>
  </define>
  <define name="Lang">
    <!--  lang as in RFC-1766 (e.g.& en-US& fr& fr-FR& etc.) -->
    <attribute name="lang">
      <a:documentation>language</a:documentation>
      <data type="language"/>
    </attribute>
  </define>
  <define name="Label">
    <!-- the text associated to a value which will be display on the form -->
    <element name="label">
      <a:documentation>label</a:documentation>
      <text/>
    </element>
  </define>
  <define name="Message">
    <!-- a message displayed when the control fail -->
    <element name="message">
      <a:documentation>message </a:documentation>
      <oneOrMore>
        <ref name="Text"/>
      </oneOrMore>
    </element>
  </define>
  <define name="Precond">
    <!--
      in parameter : the parameter is linked to a condition
      ex : this parameter could be used only if an other parameter has been specified
    -->
    <element name="precond">
      <a:documentation>relevancy condition </a:documentation>
      <oneOrMore>
        <ref name="Code"/>
      </oneOrMore>
    </element>
  </define>
  <define name="Value">
    <element name="value">
      <a:documentation>value</a:documentation>
      <text/>
    </element>
  </define>
  <define name="Code">
    <element name="code">
      <a:documentation>code snippet in a programming language</a:documentation>
      <attribute name="proglang">
        <a:documentation>programming language (e.g., python, perl, etc.)</a:documentation>
      </attribute>
      <ref name="non-empty-string"/>
    </element>
  </define>
  <define name="Text">
    <a:documentation>text element</a:documentation>
    <element name="text">
      <interleave>
        <ref name="Lang"/>
        <text/>
      </interleave>
    </element>
  </define>
  <define name="anyXHTMLorSVG">
    <a:documentation>HTML element</a:documentation>
    <zeroOrMore>
      <choice>
        <element>
          <nsName ns="http://www.w3.org/1999/xhtml"/>
          <zeroOrMore>
            <choice>
              <attribute>
                <anyName/>
              </attribute>
              <text/>
              <ref name="anyXHTMLorSVG"/>
            </choice>
          </zeroOrMore>
        </element>
        <text/>
        <element>
          <nsName ns="http://www.w3.org/2000/svg"/>
          <zeroOrMore>
            <choice>
              <attribute>
                <anyName/>
              </attribute>
              <text/>
              <ref name="anyXHTMLorSVG"/>
            </choice>
          </zeroOrMore>
        </element>
      </choice>
    </zeroOrMore>
  </define>
  <define name="anyTextOrXML">
    <choice>
      <element>
        <anyName/>
        <zeroOrMore>
          <choice>
            <attribute>
              <anyName/>
            </attribute>
            <text/>
            <ref name="anyTextOrXML"/>
          </choice>
        </zeroOrMore>
      </element>
      <text/>
    </choice>
  </define>
  <define name="Paragraph">
    <a:documentation>paragraph</a:documentation>
    <element name="paragraph">
      <interleave>
        <ref name="Name"/>
        <zeroOrMore>
          <ref name="Prompt"/>
        </zeroOrMore>
        <zeroOrMore>
          <ref name="Precond"/>
        </zeroOrMore>
        <optional>
          <ref name="Comment"/>
        </optional>
        <ref name="Parameters"/>
        <ref name="InvocationParagraph"/>
        <optional>
          <choice>
            <a:documentation>custom paragraph layout/interface</a:documentation>
            <ref name="Layout"/>
            <zeroOrMore>
              <ref name="Interface"/>
            </zeroOrMore>
          </choice>
        </optional>
      </interleave>
    </element>
  </define>
  <define name="Layout">
    <a:documentation>box-model layout</a:documentation>
    <element name="layout">
      <oneOrMore>
        <choice>
          <ref name="Hbox"/>
          <ref name="Vbox"/>
        </choice>
      </oneOrMore>
    </element>
  </define>
  <define name="Hbox">
    <a:documentation>horizontal box</a:documentation>
    <element name="hbox">
      <oneOrMore>
        <choice>
          <ref name="Box"/>
          <ref name="Layout"/>
        </choice>
      </oneOrMore>
    </element>
  </define>
  <define name="Vbox">
    <a:documentation>vertical box</a:documentation>
    <element name="vbox">
      <oneOrMore>
        <choice>
          <ref name="Box"/>
          <ref name="Layout"/>
        </choice>
      </oneOrMore>
    </element>
  </define>
  <define name="Box">
    <a:documentation>box</a:documentation>
    <element name="box">
      <text/>
    </element>
  </define>
  <define name="Interface">
    <a:documentation>custom interface specification</a:documentation>
    <element name="interface">
      <choice>
        <a:documentation>override default Choice parameter display</a:documentation>
        <attribute name="field">
          <choice>
            <value>select</value>
            <value>radio</value>
          </choice>
        </attribute>
        <interleave>
          <attribute name="type">
            <ref name="InterfaceType"/>
          </attribute>
          <optional>
            <attribute name="generated">
              <value>true</value>
            </attribute>
          </optional>
          <ref name="anyXHTMLorSVG">
            <a:documentation>override HTML display code</a:documentation>
          </ref>
        </interleave>
      </choice>
    </element>
  </define>
  <define name="Email">
    <element name="email">
      <data type="token">
        <param name="pattern">[a-z0-9\-\._]+@([a-z0-9\-]+\.)+([a-z]){2,4}</param>
      </data>
    </element>
  </define>
  <define name="non-empty-string">
    <data type="token">
      <param name="minLength">1</param>
    </data>
  </define>
  <define name="Implementation">
    <empty/>
  </define>
  <define name="InvocationHead">
    <empty/>
  </define>
  <define name="InvocationParameterElements">
    <empty/>
  </define>
  <define name="InvocationParagraph">
    <empty/>
  </define>
  <define name="InterfaceType">
    <empty/>
  </define>
</grammar>
